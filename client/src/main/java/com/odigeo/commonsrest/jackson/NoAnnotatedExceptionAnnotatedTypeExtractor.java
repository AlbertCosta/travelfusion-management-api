package com.odigeo.commonsrest.jackson;

import java.util.Collections;
import java.util.List;

public class NoAnnotatedExceptionAnnotatedTypeExtractor implements Extractor<List<Class<? extends Throwable>>> {

    @Override
    public List<Class<? extends Throwable>> extract() {
        return Collections.emptyList();
    }
}
