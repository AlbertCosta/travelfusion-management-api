package com.odigeo.commonsrest.jackson;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ExtractorFactory {

    public Extractor<List<Class<? extends Throwable>>> newAnnotatedTypeExtractor(Class<? extends Throwable> exceptionClazz) {
        Extractor extractor = null;

        Iterator<Annotation> iterator = Arrays.asList(exceptionClazz.getAnnotations()).iterator();

        while (extractor == null && iterator.hasNext()) {
            Annotation next = iterator.next();
            String annotationName = next.annotationType().getName();

            if ("org.codehaus.jackson.annotate.JsonSubTypes".equals(annotationName)) {
                extractor = new JacksonCodehausAnnotatedTypeExtractor(next);
            } else if ("com.fasterxml.jackson.annotation.JsonSubTypes".equals(annotationName)) {
                extractor = new JacksonFasterxmlAnnotatedTypeExtractor(next);
            }
        }

        if (extractor == null) {
            extractor = new NoAnnotatedExceptionAnnotatedTypeExtractor();
        }

        return extractor;
    }

    public Extractor<String> newBestNameExtractor(Class<? extends Throwable> exceptionClazz) {
        Extractor extractor = null;

        Iterator<Annotation> iterator = Arrays.asList(exceptionClazz.getAnnotations()).iterator();

        while (extractor == null && iterator.hasNext()) {
            Annotation next = iterator.next();
            String annotationName = next.annotationType().getName();

            if ("org.codehaus.jackson.annotate.JsonTypeName".equals(annotationName)) {
                extractor = new JacksonCodehausBestNameExtractor(next, exceptionClazz);
            } else if ("com.fasterxml.jackson.annotation.JsonTypeName".equals(annotationName)) {
                extractor = new JacksonFasterxmlBestNameExtractor(next, exceptionClazz);
            }
        }

        if (extractor == null) {
            extractor = new NoAnnotatedExceptionBestNameExtractor(exceptionClazz);
        }

        return extractor;
    }
}
