package com.odigeo.commonsrest.jackson;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.StringUtils;

import java.lang.annotation.Annotation;

public class JacksonFasterxmlBestNameExtractor implements Extractor<String> {

    private final JsonTypeName jsonTypeName;
    private final String bestName;

    JacksonFasterxmlBestNameExtractor(Annotation annotation, Class<? extends Throwable> exceptionClazz) {
        this.jsonTypeName = (JsonTypeName) annotation;
        this.bestName = exceptionClazz.getName();
    }

    @Override
    public String extract() {
        String result = bestName;

        String annotationValue = jsonTypeName.value();
        if (StringUtils.isNotEmpty(annotationValue)) {
            result = annotationValue;
        }

        return result;
    }
}
