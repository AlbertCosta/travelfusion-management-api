package com.odigeo.commonsrest.jackson;

public interface Extractor<T> {

    T extract();
}
