package com.odigeo.commonsrest.jackson;

public class NoAnnotatedExceptionBestNameExtractor implements Extractor<String> {

    private final String bestName;

    NoAnnotatedExceptionBestNameExtractor(Class<? extends Throwable> exceptionClazz) {
        bestName = exceptionClazz.getName();
    }

    @Override
    public String extract() {
        return bestName;
    }
}
