package com.odigeo.commonsrest.jackson;

import org.codehaus.jackson.annotate.JsonSubTypes;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

public class JacksonCodehausAnnotatedTypeExtractor implements Extractor<List<Class<? extends Throwable>>> {

    private final JsonSubTypes jsonSubTypes;

    JacksonCodehausAnnotatedTypeExtractor(Annotation annotation) {
        jsonSubTypes = (JsonSubTypes) annotation;
    }

    @Override
    public List<Class<? extends Throwable>> extract() {
        List<Class<? extends Throwable>> exceptionList = new ArrayList<Class<? extends Throwable>>();

        for (JsonSubTypes.Type type : jsonSubTypes.value()) {
            exceptionList.add((Class<? extends Throwable>) type.value());
        }

        return exceptionList;
    }
}
