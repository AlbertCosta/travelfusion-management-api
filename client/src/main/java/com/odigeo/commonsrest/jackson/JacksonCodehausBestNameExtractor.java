package com.odigeo.commonsrest.jackson;


import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.lang.annotation.Annotation;

public class JacksonCodehausBestNameExtractor implements Extractor<String> {

    private final JsonTypeName jsonTypeName;
    private final String bestName;

    JacksonCodehausBestNameExtractor(Annotation annotation, Class<? extends Throwable> exceptionClazz) {
        this.jsonTypeName = (JsonTypeName) annotation;
        this.bestName = exceptionClazz.getName();
    }

    @Override
    public String extract() {
        String result = bestName;

        String annotationValue = jsonTypeName.value();
        if (StringUtils.isNotEmpty(annotationValue)) {
            result = annotationValue;
        }

        return result;
    }
}
