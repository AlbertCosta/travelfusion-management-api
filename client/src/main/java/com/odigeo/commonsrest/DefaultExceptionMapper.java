package com.odigeo.commonsrest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Use this in the server side when implementing a contract interface to map thrown exceptions to BAD_REQUEST responses
 * with a JSON body representing the exception thrown. The RestErrorHandler associated by default to client proxies
 * constructed by ServiceBuilder knows how to deal with these.
 *
 * @author carlos.aller@odigeo.com
 * @since 30/01/14
 */
public abstract class DefaultExceptionMapper<T extends Exception> { // It cannot implement ExceptionMapper, or RESTEasy won't detect it properly.

    public final Response toResponse(T exception) {
        return Response.status(statusToSend()).type(mediaTypeToSend()).entity(beanToSend(exception)).build();
    }

    /**
     * Override this to control whether or not the exception "history trace" is sent in the response
     */
    public boolean sendExceptionCause() {
        return false;
    }

    /**
     * Override this to control the error code sent in the response for this exception
     */
    protected Response.Status statusToSend() {
        return Response.Status.BAD_REQUEST;
    }

    /**
     * Override this to control the Content-Type of the response for this exception
     */
    protected String mediaTypeToSend() {
        return MediaType.APPLICATION_JSON;
    }

    protected SimpleExceptionBean beanToSend(T exception) {
        return new SimpleExceptionBean(exception, sendExceptionCause());
    }
}
