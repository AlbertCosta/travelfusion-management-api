package com.odigeo.commonsrest;

import com.odigeo.commons.rest.RestExceptionBean;
import com.odigeo.commonsrest.jackson.ExtractorFactory;

/**
 * An implementation of the GenericExceptionBean thought for the internal
 * Odigeo convention where we pass the type of exception and a message.
 * Users can override the toException method to generate custom exceptions
 *
 * @author carlos.aller@odigeo.com
 * @since 27/01/14
 */
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleExceptionBean implements RestExceptionBean {

    private String type;
    private String message;
    private SimpleExceptionBean cause;

    public SimpleExceptionBean(Throwable throwable, boolean includeCause) {
        this.type = new ExtractorFactory().newBestNameExtractor(throwable.getClass()).extract();
        this.message = throwable.getMessage();
        Throwable cause = throwable.getCause();
        if (includeCause && cause != null) {
            this.cause = new SimpleExceptionBean(cause, true);
        }
    }

    public SimpleExceptionBean() {
    }

    public final String getType() {
        return type;
    }

    public final void setType(String type) {
        this.type = type;
    }

    public final String getMessage() {
        return message;
    }

    public final void setMessage(String message) {
        this.message = message;
    }

    public final SimpleExceptionBean getCause() {
        return cause;
    }

    public final void setCause(SimpleExceptionBean cause) {
        this.cause = cause;
    }

    @Override
    public Exception toException() {
        return new UnsupportedOperationException("Could not generate proper exception with type=" + type + " and message = " + message);
    }
}
