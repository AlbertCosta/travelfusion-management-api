package com.odigeo.commonsrest;

import com.odigeo.commons.rest.RestExceptionBean;
import com.odigeo.commons.rest.error.BaseRestErrorsHandler;
import com.odigeo.commons.rest.error.RestWrapperException;
import com.odigeo.commonsrest.jackson.Extractor;
import com.odigeo.commonsrest.jackson.ExtractorFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author carlos.aller@odigeo.com
 * @since 29/01/14
 */
public class SimpleRestErrorsHandler extends BaseRestErrorsHandler {
    private static final ConcurrentHashMap<Class<?>, Map<String, Class<? extends Throwable>>> INTROSPECTED_EXCEPTIONS = new ConcurrentHashMap<Class<?>, Map<String, Class<? extends Throwable>>>();

    private final Class serviceClass;

    public SimpleRestErrorsHandler(Class serviceClass) {
        super(SimpleExceptionBean.class);
        this.serviceClass = serviceClass;
    }

    private Map<String, Class<? extends Throwable>> introspectExceptions(Class serviceClass) {
        Map<String, Class<? extends Throwable>> cachedValue = INTROSPECTED_EXCEPTIONS.get(serviceClass);
        if (cachedValue != null) {
            return cachedValue;
        }
        Map<String, Class<? extends Throwable>> mappings = new HashMap<String, Class<? extends Throwable>>();
        Method[] methods = serviceClass.getDeclaredMethods();
        for (Method method : methods) {
            Class<?>[] exceptions = method.getExceptionTypes();
            for (Class<?> exception : exceptions) {
                if (Throwable.class.isAssignableFrom(exception)) {
                    addExceptionToMappings((Class<? extends Throwable>) exception, mappings);
                }
            }
        }
        INTROSPECTED_EXCEPTIONS.put(serviceClass, mappings);
        return mappings;
    }

    private void addExceptionToMappings(Class<? extends Throwable> exceptionClazz, Map<String, Class<? extends Throwable>> mappings) {
        // add also short name for compatibility with old API's like GEO-API
        mappings.put(exceptionClazz.getSimpleName(), exceptionClazz);
        mappings.put(getBestName(exceptionClazz), exceptionClazz);

        Extractor<List<Class<? extends Throwable>>> extractor = new ExtractorFactory().newAnnotatedTypeExtractor(exceptionClazz);
        for (Class<? extends Throwable> exceptionClassIterable : extractor.extract()) {
            addExceptionToMappings(exceptionClassIterable, mappings);
        }
    }

    private String getBestName(Class<? extends Throwable> exceptionClazz) {
        return new ExtractorFactory().newBestNameExtractor(exceptionClazz).extract();
    }

    protected void throwException(RestExceptionBean data) {
        throw new RestWrapperException(getException((SimpleExceptionBean) data));
    }

    @SuppressWarnings("PMD.InsufficientStringBufferDeclaration")
    private Throwable getReconstructedUnrecognizedException(String genericMessage, SimpleExceptionBean exceptionBean) {
        StringBuilder reconstructedStackMessage = new StringBuilder();
        reconstructedStackMessage.append(genericMessage);
        SimpleExceptionBean cause = exceptionBean.getCause();
        while (cause != null) {
            String causeMessage = cause.getMessage();
            if (causeMessage != null) {
                causeMessage = causeMessage.trim();
            }
            reconstructedStackMessage.append("\nCaused by ").append(cause.getType()).append(": ").append(causeMessage).append("\n    (...remote host trace unknown...)");
            cause = cause.getCause();
        }
        return new Throwable(reconstructedStackMessage.toString());
    }

    private Throwable getRecognizedException(String genericMessage, SimpleExceptionBean exceptionBean, Class<? extends Throwable> clazz) {
        try {
            Throwable e;
            Constructor<? extends Throwable> fullConstructor = getConstructorWithSignature(clazz, String.class, Throwable.class);
            Constructor<? extends Throwable> messageConstructor = getConstructorWithSignature(clazz, String.class);
            Constructor<? extends Throwable> emptyConstructor = getConstructorWithSignature(clazz);

            if (fullConstructor != null) {
                if (exceptionBean.getCause() != null) {
                    Throwable realCause = getException(exceptionBean.getCause());
                    e = fullConstructor.newInstance(exceptionBean.getMessage(), realCause);
                } else {
                    e = fullConstructor.newInstance(exceptionBean.getMessage(), null);
                }
            } else if (messageConstructor != null) {
                e = messageConstructor.newInstance(exceptionBean.getMessage());
            } else if (emptyConstructor != null) {
                e = emptyConstructor.newInstance();
            } else {
                e = exceptionBean.toException();
            }
            return e;
        } catch (InstantiationException e) {
            return new Throwable(genericMessage, e);
        } catch (IllegalAccessException e) {
            return new Throwable(genericMessage, e);
        } catch (InvocationTargetException e) {
            return new Throwable(genericMessage, e);
        }
    }

    //TODO: Check why RuntimeExceptions are encapsulated into UndeclaredThrowableException
    private Throwable getException(SimpleExceptionBean exceptionBean) {
        Map<String, Class<? extends Throwable>> exceptionMappings = introspectExceptions(serviceClass);
        Class<? extends Throwable> clazz = exceptionMappings.get(exceptionBean.getType());
        String genericMessage = exceptionBean.getType() + ": " + exceptionBean.getMessage();
        if (clazz == null) {
            return getReconstructedUnrecognizedException(genericMessage, exceptionBean);
        } else {
            return getRecognizedException(genericMessage, exceptionBean, clazz);
        }
    }

    private Constructor<? extends Throwable> getConstructorWithSignature(Class<? extends Throwable> clazz, Class<?>... args) {
        try {
            return clazz.getConstructor(args);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }
}
