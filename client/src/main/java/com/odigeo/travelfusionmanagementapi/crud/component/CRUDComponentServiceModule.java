package com.odigeo.travelfusionmanagementapi.crud.component;

import com.google.common.collect.ImmutableSet;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.cache.CacheInterceptor;
import com.odigeo.commons.rest.cache.ClientCacheConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.travelfusionmanagementapi.AbstractServiceModule;
import com.odigeo.travelfusionmanagementapi.cache.SimpleClientCacheConfiguration;

import java.util.Set;

public class CRUDComponentServiceModule extends AbstractServiceModule<CRUDComponentService> {

    public CRUDComponentServiceModule(ServiceNotificator... notificators) {
        super(CRUDComponentService.class, notificators);
    }

    protected CacheInterceptor initLocalCacheInterceptor() {
        SimpleClientCacheConfiguration simpleClientCacheConfiguration = new SimpleClientCacheConfiguration<>(CRUDComponentService.class);
        Set<ClientCacheConfiguration> clientCacheConfigurations = ImmutableSet.of(
                simpleClientCacheConfiguration.createClientCacheConfiguration("getComponent"));
        return new CacheInterceptor(clientCacheConfigurations);
    }

    @Override
    protected void initCacheConfiguration(ServiceConfiguration.Builder<CRUDComponentService> builder, InterceptorConfiguration<CRUDComponentService> interceptorConfiguration) {
        if (!isRedisAllowed()) {
            interceptorConfiguration.addInterceptor(initLocalCacheInterceptor());
        }
    }
}
