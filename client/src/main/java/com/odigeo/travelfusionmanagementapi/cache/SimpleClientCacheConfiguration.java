package com.odigeo.travelfusionmanagementapi.cache;

import com.edreams.persistance.cache.impl.SimpleMemoryOnlyCache;
import com.odigeo.commons.rest.cache.SerializableCache;
import java.io.Serializable;

public class SimpleClientCacheConfiguration<T> {

    private static final String STORAGE_SPACE_CACHE = "TravelFusionManagementClientCache";

    private final Class<T> tClass;
    private final SerializableCache serializableCache;

    public SimpleClientCacheConfiguration(Class<T> tClass) {
        this.tClass = tClass;
        this.serializableCache = new SerializableCache(new SimpleMemoryOnlyCache(STORAGE_SPACE_CACHE, getKeyFromClassName(tClass), Serializable.class));
    }

    private SerializableCache getClientCache() {
        return serializableCache;
    }

    public TravelFusionManagementClientCacheConfiguration<T> createClientCacheConfiguration(String methodName) {
        return new TravelFusionManagementClientCacheConfiguration<T>(tClass, methodName, getClientCache());
    }

    private String getKeyFromClassName(Class tClass) {
        return tClass.getName().replace(".", "_");
    }
}
