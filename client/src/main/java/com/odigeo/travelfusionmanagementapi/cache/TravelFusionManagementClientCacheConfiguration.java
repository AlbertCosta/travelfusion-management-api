package com.odigeo.travelfusionmanagementapi.cache;

import com.odigeo.commons.rest.cache.ClientCache;
import com.odigeo.commons.rest.cache.ClientCacheConfiguration;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Iterator;

public class TravelFusionManagementClientCacheConfiguration<T> implements ClientCacheConfiguration {

    private final Class<T> aClass;
    private final String methodName;
    private final ClientCache clientCache;

    public TravelFusionManagementClientCacheConfiguration(Class<T> aClass, String methodName, ClientCache clientCache) {
        this.aClass = aClass;
        this.methodName = methodName;
        this.clientCache = clientCache;
    }

    @Override
    public ClientCache getClientCache() {
        return clientCache;
    }

    @Override
    public Class getClazz() {
        return aClass;
    }

    @Override
    public Method getMethod() {
        Method result = null;
        Iterator<Method> iterator = Arrays.asList(aClass.getMethods()).iterator();

        while (iterator.hasNext()
                && result == null) {
            Method method = iterator.next();
            if (method.getName().equals(methodName)) {
                result = method;
            }
        }

        if (result == null) {
            throw new IllegalStateException("Method: " + methodName + " not found in " + aClass);
        }

        return result;
    }

    @Override
    public String getKey() {
        return null;
    }
}
