package com.odigeo.travelfusionmanagementapi;

import com.odigeo.commons.rest.error.RestWrapperException;
import com.odigeo.travelfusionmanagementapi.exception.TravelFusionConfigurationNotFoundException;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.spi.interception.ClientExecutionContext;
import org.jboss.resteasy.spi.interception.ClientExecutionInterceptor;

import javax.ws.rs.core.Response;

public class TravelFusionManagementClientExecutionInterceptor implements ClientExecutionInterceptor {

    @Override
    @SuppressWarnings("PMD.SignatureDeclareThrowsException")
    public ClientResponse execute(ClientExecutionContext clientExecutionContext) throws Exception {
        ClientResponse clientResponse = clientExecutionContext.proceed();

        if (clientResponse.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            TravelFusionConfigurationNotFoundException providerException = new TravelFusionConfigurationNotFoundException("TravelFusion configuration not found for " + clientExecutionContext.getRequest().getUri());
            throw new RestWrapperException(providerException);
        }
        return clientResponse;
    }
}
