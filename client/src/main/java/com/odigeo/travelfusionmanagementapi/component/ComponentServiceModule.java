package com.odigeo.travelfusionmanagementapi.component;

import com.google.common.collect.ImmutableSet;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.cache.CacheInterceptor;
import com.odigeo.commons.rest.cache.ClientCacheConfiguration;
import com.odigeo.travelfusionmanagementapi.AbstractServiceModule;
import com.odigeo.travelfusionmanagementapi.cache.SimpleClientCacheConfiguration;

import java.util.Set;

public class ComponentServiceModule extends AbstractServiceModule<ComponentService> {

    public ComponentServiceModule(ServiceNotificator... notificators) {
        super(ComponentService.class, notificators);
    }

    protected CacheInterceptor initLocalCacheInterceptor() {
        SimpleClientCacheConfiguration simpleClientCacheConfiguration = new SimpleClientCacheConfiguration<>(ComponentService.class);
        Set<ClientCacheConfiguration> clientCacheConfigurations = ImmutableSet.of(
                simpleClientCacheConfiguration.createClientCacheConfiguration("getComponent"),
                simpleClientCacheConfiguration.createClientCacheConfiguration("getComponentVersion"),
                simpleClientCacheConfiguration.createClientCacheConfiguration("isWebsiteForcingToBeMerchant"),
                simpleClientCacheConfiguration.createClientCacheConfiguration("agentParameter"));
        return new CacheInterceptor(clientCacheConfigurations);
    }
}
