package com.odigeo.travelfusionmanagementapi.mapper;

import com.odigeo.commonsrest.DefaultExceptionMapper;
import com.odigeo.commonsrest.SimpleExceptionBean;
import com.odigeo.travelfusionmanagementapi.exception.TravelFusionManagementException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RuntimeExceptionRestMapper extends DefaultExceptionMapper<RuntimeException> implements ExceptionMapper<RuntimeException> {

    @Override
    protected Response.Status statusToSend() {
        return Response.Status.INTERNAL_SERVER_ERROR;
    }

    @Override
    public boolean sendExceptionCause() {
        return true;
    }

    @Override
    protected SimpleExceptionBean beanToSend(RuntimeException exception) {
        return new SimpleExceptionBean(new TravelFusionManagementException("Runtime mapped to Generic Exception", exception), sendExceptionCause());
    }
}
