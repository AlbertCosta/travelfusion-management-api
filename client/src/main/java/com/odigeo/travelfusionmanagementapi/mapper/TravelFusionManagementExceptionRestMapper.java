package com.odigeo.travelfusionmanagementapi.mapper;

import com.odigeo.commonsrest.DefaultExceptionMapper;
import com.odigeo.travelfusionmanagementapi.exception.TravelFusionManagementException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class TravelFusionManagementExceptionRestMapper extends DefaultExceptionMapper<TravelFusionManagementException>
        implements ExceptionMapper<TravelFusionManagementException> {

    @Override
    protected Response.Status statusToSend() {
        return Response.Status.BAD_REQUEST;
    }

    @Override
    public boolean sendExceptionCause() {
        return true;
    }
}
