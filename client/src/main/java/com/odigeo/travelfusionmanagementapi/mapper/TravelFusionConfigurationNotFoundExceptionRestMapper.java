package com.odigeo.travelfusionmanagementapi.mapper;

import com.odigeo.commonsrest.DefaultExceptionMapper;
import com.odigeo.travelfusionmanagementapi.exception.TravelFusionConfigurationNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class TravelFusionConfigurationNotFoundExceptionRestMapper extends DefaultExceptionMapper<TravelFusionConfigurationNotFoundException>
        implements ExceptionMapper<TravelFusionConfigurationNotFoundException> {

    @Override
    protected Response.Status statusToSend() {
        return Response.Status.NOT_FOUND;
    }

    @Override
    public boolean sendExceptionCause() {
        return true;
    }
}
