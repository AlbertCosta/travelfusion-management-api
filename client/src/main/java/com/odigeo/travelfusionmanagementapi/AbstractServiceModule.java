package com.odigeo.travelfusionmanagementapi;

import com.odigeo.commons.monitoring.metrics.configuration.EnvironmentPropertiesRetriever;
import com.odigeo.commons.monitoring.metrics.configuration.PlatformResolver;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.cache.CacheInterceptor;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.guice.DefaultRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.JacksonVersion;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.commonsrest.SimpleRestErrorsHandler;
import com.odigeo.trackingapi.client.v2.context.ExecutionContextSenderResteasyFactory;

public abstract class AbstractServiceModule<T> extends DefaultRestUtilsModule<T> {

    private static final int SOCKET_TIMEOUT = 1000;

    private static final int TIMEOUT_CONNECTION = 50;

    private final Class<T> tClass;

    private final PlatformResolver platformResolver;

    public AbstractServiceModule(Class<T> tClass, ServiceNotificator... notificators) {
        super(tClass, notificators);
        this.tClass = tClass;
        this.platformResolver = new PlatformResolver(new EnvironmentPropertiesRetriever());
    }

    @Override
    public ServiceConfiguration<T> getServiceConfiguration(Class<T> serviceContractClass) {
        ServiceConfiguration.Builder<T> builder = getBasicConfiguration(serviceContractClass);
        InterceptorConfiguration<T> interceptorConfiguration = initInterceptorConfiguration();
        initCacheConfiguration(builder, interceptorConfiguration);
        builder.withInterceptorConfiguration(interceptorConfiguration);
        return builder.build();
    }

    protected void initCacheConfiguration(ServiceConfiguration.Builder<T> builder, InterceptorConfiguration<T> interceptorConfiguration) {
        if (isRedisAllowed()) {
            builder.withHttpCache(SOCKET_TIMEOUT, TIMEOUT_CONNECTION);
        } else {
            interceptorConfiguration.addInterceptor(initLocalCacheInterceptor());
        }
    }

    protected abstract CacheInterceptor initLocalCacheInterceptor();

    protected boolean isRedisAllowed() {
        return platformResolver.isKubernetes();
    }

    private ServiceConfiguration.Builder<T> getBasicConfiguration(Class<T> serviceContractClass) {
        return new ServiceConfiguration.Builder<>(serviceContractClass)
                .withJacksonVersion(JacksonVersion.V_2_6_0)
                .withFaultTolerance()
                .withInterceptorConfiguration(initInterceptorConfiguration())
                .withRestErrorsHandler(new SimpleRestErrorsHandler(tClass));
    }

    private InterceptorConfiguration<T> initInterceptorConfiguration() {
        InterceptorConfiguration<T> interceptorConfiguration = new InterceptorConfiguration<>();
        interceptorConfiguration.addInterceptor(ExecutionContextSenderResteasyFactory.getInstance());
        interceptorConfiguration.addInterceptor(new TravelFusionManagementClientExecutionInterceptor());
        return interceptorConfiguration;
    }
}
