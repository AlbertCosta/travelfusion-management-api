package com.odigeo.travelfusionmanagementapi.route;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.cache.CacheInterceptor;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.travelfusionmanagementapi.AbstractServiceModule;

public class RouteServiceModule extends AbstractServiceModule<RouteService> {

    public RouteServiceModule(ServiceNotificator... notificators) {
        super(RouteService.class, notificators);
    }

    @Override
    protected void initCacheConfiguration(ServiceConfiguration.Builder<RouteService> builder, InterceptorConfiguration<RouteService> interceptorConfiguration) {
        // Overriding to do nothing, because we don't want use cache to routes service
    }

    @Override
    protected CacheInterceptor initLocalCacheInterceptor() {
        throw new UnsupportedOperationException("This method is invoke by initCacheConfiguration, therefore should be never be invoked that is empty in this module");
    }
}
