package com.odigeo.travelfusionmanagementapi.cache;

import com.odigeo.commons.rest.cache.ClientCache;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class TravelFusionManagementClientCacheConfigurationTest {

    @Mock
    private ClientCache clientCache;
    private TravelFusionManagementClientCacheConfiguration<DummyClass> travelFusionManagementClientCacheConfiguration;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetMethod() {
        travelFusionManagementClientCacheConfiguration = new TravelFusionManagementClientCacheConfiguration<>(DummyClass.class, "method", clientCache);
        assertNotNull(travelFusionManagementClientCacheConfiguration.getMethod());
    }


    @Test(expectedExceptions = IllegalStateException.class)
    public void testGetMethodThrownIllegalStateException() {
        travelFusionManagementClientCacheConfiguration = new TravelFusionManagementClientCacheConfiguration<>(DummyClass.class, "NonExistingMethod", clientCache);
        travelFusionManagementClientCacheConfiguration.getMethod();
    }
}
