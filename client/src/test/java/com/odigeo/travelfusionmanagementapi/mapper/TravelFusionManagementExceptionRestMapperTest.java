package com.odigeo.travelfusionmanagementapi.mapper;

import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

import static org.testng.Assert.assertEquals;

public class TravelFusionManagementExceptionRestMapperTest {

    @Test
    public void testStatusToSend() {
        assertEquals(new TravelFusionManagementExceptionRestMapper().statusToSend(), Response.Status.BAD_REQUEST);
    }
}
