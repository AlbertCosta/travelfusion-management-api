package com.odigeo.travelfusionmanagementapi.mapper;

import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

import static org.testng.Assert.assertEquals;

public class TravelFusionConfigurationNotFoundExceptionRestMapperTest {

    @Test
    public void testStatusToSend() {
        assertEquals(new TravelFusionConfigurationNotFoundExceptionRestMapper().statusToSend(), Response.Status.NOT_FOUND);
    }
}
