package com.odigeo.travelfusionmanagementapi.mapper;

import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

import static org.testng.Assert.assertEquals;

public class RuntimeExceptionRestMapperTest {

    @Test
    public void testStatusToSend() {
        assertEquals(new RuntimeExceptionRestMapper().statusToSend(), Response.Status.INTERNAL_SERVER_ERROR);
    }
}
