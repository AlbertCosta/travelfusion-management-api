package com.odigeo.travelfusionmanagementapi.route;

import com.odigeo.commons.monitoring.metrics.configuration.PlatformResolver;
import com.odigeo.commons.rest.cache.CacheInterceptor;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class RouteServiceModuleTest {


    private RouteServiceModule routeServiceModule;


    @Mock
    private PlatformResolver platformResolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        routeServiceModule = new RouteServiceModule();
        Whitebox.setInternalState(routeServiceModule, "platformResolver", platformResolver);
    }

    @Test
    public void testGetConfiguration() {
        ServiceConfiguration<RouteService> serviceConfiguration = routeServiceModule.getServiceConfiguration(RouteService.class);
        assertNotNull(serviceConfiguration);
    }

    @Test
    public void testLocalCacheConfiguration() {
        when(platformResolver.isKubernetes()).thenReturn(false);
        ServiceConfiguration<RouteService> serviceConfiguration = routeServiceModule.getServiceConfiguration(RouteService.class);
        assertNotNull(serviceConfiguration);
        assertTrue(serviceConfiguration.getInterceptorConfiguration().getInterceptors().stream().noneMatch(interceptor -> interceptor instanceof CacheInterceptor));
    }

    @Test
    public void testNonLocalCacheConfiguration() {
        when(platformResolver.isKubernetes()).thenReturn(true);
        ServiceConfiguration<RouteService> serviceConfiguration = routeServiceModule.getServiceConfiguration(RouteService.class);
        assertNotNull(serviceConfiguration);
        assertTrue(serviceConfiguration.getInterceptorConfiguration().getInterceptors().stream().noneMatch(interceptor -> interceptor instanceof CacheInterceptor));
    }

}
