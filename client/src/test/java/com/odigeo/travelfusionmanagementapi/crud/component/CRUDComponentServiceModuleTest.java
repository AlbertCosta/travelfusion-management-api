package com.odigeo.travelfusionmanagementapi.crud.component;

import com.odigeo.commons.monitoring.metrics.configuration.PlatformResolver;
import com.odigeo.commons.rest.cache.CacheInterceptor;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class CRUDComponentServiceModuleTest {

    @Mock
    private PlatformResolver platformResolver;

    private CRUDComponentServiceModule crudComponentServiceModule;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        crudComponentServiceModule = new CRUDComponentServiceModule();
        Whitebox.setInternalState(crudComponentServiceModule, "platformResolver", platformResolver);
    }

    @Test
    public void testSetUp() {
        ServiceConfiguration<CRUDComponentService> serviceConfiguration = crudComponentServiceModule.getServiceConfiguration(CRUDComponentService.class);
        assertTrue(serviceConfiguration.getInterceptorConfiguration().getInterceptors().stream().anyMatch(interceptor -> interceptor instanceof CacheInterceptor));
        assertNotNull(serviceConfiguration);
    }

    @Test
    public void testLocalCacheConfiguration() {
        when(platformResolver.isKubernetes()).thenReturn(false);
        ServiceConfiguration<CRUDComponentService> serviceConfiguration = crudComponentServiceModule.getServiceConfiguration(CRUDComponentService.class);
        assertNotNull(serviceConfiguration);
        assertTrue(serviceConfiguration.getInterceptorConfiguration().getInterceptors().stream().anyMatch(interceptor -> interceptor instanceof CacheInterceptor));
    }

    @Test
    public void testNonLocalCacheConfiguration() {
        when(platformResolver.isKubernetes()).thenReturn(true);
        ServiceConfiguration<CRUDComponentService> serviceConfiguration = crudComponentServiceModule.getServiceConfiguration(CRUDComponentService.class);
        assertNotNull(serviceConfiguration);
        assertTrue(serviceConfiguration.getInterceptorConfiguration().getInterceptors().stream().noneMatch(interceptor -> interceptor instanceof CacheInterceptor));
    }
}
