package com.odigeo.travelfusionmanagementapi.component;

import com.odigeo.commons.test.random.Picker;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

public class ComponentServiceMockTest {

    private static final int MAX_RESULTS_NUMBER = 5;
    private ComponentServiceMock componentServiceMock;

    @BeforeMethod
    public void init() {
        Picker picker = new Picker(new Random());
        componentServiceMock = new ComponentServiceMock(picker.pickLong(0, MAX_RESULTS_NUMBER));
    }

    @Test
    public void testGetComponent() {
        Component component = componentServiceMock.getComponent("FR");
        assertNotNull(component);
    }

    @Test
    public void testGetComponentVersion() {
        ComponentVersion componentVersion = componentServiceMock.getComponentVersion("FR", "ES", "ES");
        assertNotNull(componentVersion);
    }

    @Test
    public void testGetAllComponents() {
        List<Component> components = componentServiceMock.getAllComponents();
        assertFalse(components.isEmpty());
    }

    @Test
    public void testGetAgentParameter() {
        AgentParameter agentParameter = componentServiceMock.agentParameter("ES", "FR");
        assertNotNull(agentParameter);
    }
}
