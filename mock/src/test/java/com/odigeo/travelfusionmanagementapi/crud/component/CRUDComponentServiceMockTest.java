package com.odigeo.travelfusionmanagementapi.crud.component;

import com.odigeo.commons.test.random.Picker;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

public class CRUDComponentServiceMockTest {

    private static final int MAX_RESULTS_NUMBER = 5;

    private CRUDComponentServiceMock crudComponentServiceMock;

    @BeforeMethod
    public void init() {
        Picker picker = new Picker(new Random());
        crudComponentServiceMock = new CRUDComponentServiceMock(picker.pickLong(0, MAX_RESULTS_NUMBER));
    }

    @Test
    public void testGetComponent() {
        Component component = crudComponentServiceMock.getComponent("FR");
        assertNotNull(component);
    }

    @Test
    public void testGetAllComponents() {
        List<Component> components = crudComponentServiceMock.getAllComponents();
        assertFalse(components.isEmpty());
    }

}
