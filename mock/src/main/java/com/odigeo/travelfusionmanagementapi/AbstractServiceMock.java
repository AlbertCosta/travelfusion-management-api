package com.odigeo.travelfusionmanagementapi;

import com.odigeo.pupulator.RandomPopulatorBuilder;
import com.odigeo.pupulator.RandomPopulatorBuilderFactory;

public abstract class AbstractServiceMock {

    protected final RandomPopulatorBuilder randomPopulatorBuilder;

    public AbstractServiceMock(long seed) {
        randomPopulatorBuilder = RandomPopulatorBuilderFactory.aNewBuilder(seed)
                .onlyNullableAnnotatedFieldCouldBeNull(true);
    }
}
