package com.odigeo.travelfusionmanagementapi.component;

import com.odigeo.travelfusionmanagementapi.AbstractServiceMock;

import java.util.List;

public class ComponentServiceMock extends AbstractServiceMock implements ComponentService {

    public ComponentServiceMock(long seed) {
        super(seed);
    }

    @Override
    public Component getComponent(String code) {
        return randomPopulatorBuilder.randomizeFieldFixedValue("code", String.class, Component.class, code)
                .build()
                .nextObject(Component.class);
    }

    @Override
    public ComponentVersion getComponentVersion(String componentCode, String websiteCode, String countryCode) {
        return randomPopulatorBuilder.build()
                .nextObject(ComponentVersion.class);
    }

    @Override
    public List<Component> getAllComponents() {
        return randomPopulatorBuilder.build()
                .nextObjectList(Component.class);
    }

    @Override
    public boolean isWebsiteForcingToBeMerchant(String componentCode, String websiteCode) {
        return randomPopulatorBuilder.build()
                .nextBoolean();
    }

    @Override
    public AgentParameter agentParameter(String countryCode, String componentId) {
        return randomPopulatorBuilder.build()
                .nextObject(AgentParameter.class);
    }

    @Override
    public List<ComponentStatus> getAllComponentStatus() {
        return randomPopulatorBuilder.build()
                .nextObjectList(ComponentStatus.class);
    }

    @Override
    public List<ComponentStatus> getComponentStatusByComponent(String componentId) {
        return randomPopulatorBuilder.build()
                .nextObjectList(ComponentStatus.class);
    }

    @Override
    public ComponentStatus getComponentStatusByComponentAndWebsite(String componentId, String websiteCode) {
        return randomPopulatorBuilder.build()
                .nextObject(ComponentStatus.class);
    }
}
