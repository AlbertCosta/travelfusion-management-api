package com.odigeo.travelfusionmanagementapi.crud.component;

import com.google.inject.Inject;
import com.odigeo.commons.test.random.RandomBuilder;
import com.odigeo.travelfusionmanagementapi.AbstractServiceMock;

import java.util.List;

public class CRUDComponentServiceMock extends AbstractServiceMock implements CRUDComponentService {

    public CRUDComponentServiceMock(long seed) {
        super(seed);
    }

    @Inject
    public CRUDComponentServiceMock(RandomBuilder randomBuilder) {
        this(randomBuilder.getSeed());
    }

    @Override
    public Component getComponent(String code) {
        return randomPopulatorBuilder.randomizeFieldFixedValue("code", String.class, Component.class, code)
                .build()
                .nextObject(Component.class);
    }

    @Override
    public List<Component> getAllComponents() {
        return randomPopulatorBuilder.build()
                .nextObjectList(Component.class);
    }
}
