package com.odigeo.travelfusionmanagementapi.route;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RouteSerializerTest {

    private static final String DEPARTURE = "LON";
    private static final String ARRIVAL = "MAD";
    private static final String DELIMITER = "-";

    @Test
    public void testToJson() {
        Route route = Route.builder().departureCity(DEPARTURE).arrivalCity(ARRIVAL).build();

        String routeJson = route.toString();

        Assert.assertNotNull(routeJson);
        Assert.assertTrue(routeJson.contains(DEPARTURE));
        Assert.assertTrue(routeJson.contains(DELIMITER));
        Assert.assertTrue(routeJson.contains(ARRIVAL));
    }

    @Test
    public void testFromJson() {
        String routeJson = DEPARTURE + DELIMITER + ARRIVAL;

        Route route = new Route(routeJson);

        Assert.assertNotNull(route);
        Assert.assertTrue(DEPARTURE.equals(route.getDepartureCity()));
        Assert.assertTrue(ARRIVAL.equals(route.getArrivalCity()));
    }

}
