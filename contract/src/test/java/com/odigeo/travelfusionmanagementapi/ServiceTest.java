package com.odigeo.travelfusionmanagementapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.reflect.ClassPath;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import java.io.IOException;

import static org.testng.Assert.assertTrue;

public class ServiceTest {

    private static final Logger LOGGER = Logger.getLogger(ServiceTest.class);
    private static final String CONTRACT_PACKAGE = ServiceTest.class.getPackage().getName();

    @Test
    public void checkJsonIgnorePropertiesAnnotation() throws IOException {
        boolean isCorrect = true;

        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        for (ClassPath.ClassInfo info : ClassPath.from(loader).getTopLevelClasses()) {
            if (info.getName().startsWith(CONTRACT_PACKAGE)) {
                Class<?> aClass = info.load();

                if (!aClass.getSimpleName().contains("Test")
                        && !(aClass.getSimpleName().endsWith("Service")
                        && aClass.isInterface())) {
                    JsonIgnoreProperties annotation = aClass.getAnnotation(JsonIgnoreProperties.class);
                    if (annotation == null || !annotation.ignoreUnknown()) {
                        LOGGER.error("Class: " + aClass.getCanonicalName() + " must contains @JsonIgnoreProperties(ignoreUnknown = true).");
                        isCorrect = false;
                    }
                }
            }
        }

        assertTrue(isCorrect);
    }
}
