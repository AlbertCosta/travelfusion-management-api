package com.odigeo.travelfusionmanagementapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum ProductType {
    TRAIN, FLIGHT

}
