package com.odigeo.travelfusionmanagementapi.component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ComponentStatus implements Serializable {

    private static final long serialVersionUID = -4758221910467918737L;

    private String componentId;
    private String websiteCode;
    private Status status;
}
