package com.odigeo.travelfusionmanagementapi.component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Component implements Serializable {

    private static final long serialVersionUID = 2321067631182888036L;

    private String code;
    private int maxNumberOfBags;
    private int minNumberOfBags;
    private int maxBaggageWeight;
    private int minBaggageWeight;
    private String gdsIdBackup;
    private boolean api3DSecure;
    private boolean merchant3DSecure;
    private boolean merchantAllowed;

}
