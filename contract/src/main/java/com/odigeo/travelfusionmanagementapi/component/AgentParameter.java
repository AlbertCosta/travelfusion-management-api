package com.odigeo.travelfusionmanagementapi.component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgentParameter implements Serializable {

    private static final long serialVersionUID = -7286964227948852957L;

    private String country;
    private String component;
    private int agentIataNumber;
    private String agentIdentifier;
    private String agentLogin;
    private String agentPassword;
}
