package com.odigeo.travelfusionmanagementapi.component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum PrepaymentType {
    ALWAYS,
    WHEN_APPLICABLE,
    NEVER;
}
