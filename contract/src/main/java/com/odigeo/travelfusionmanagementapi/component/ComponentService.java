package com.odigeo.travelfusionmanagementapi.component;

import com.odigeo.commons.rest.ServiceGroup;
import com.odigeo.commons.rest.metadata.Metadata;
import com.odigeo.travelfusionmanagementapi.exception.TravelFusionConfigurationNotFoundException;
import com.odigeo.travelfusionmanagementapi.exception.TravelFusionManagementException;
import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.cache.Cache;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/v1/components")
@ServiceGroup("TravelFusionManagement")
@Cache(maxAge = 1800)
public interface ComponentService {

    String JSON_MIME_TYPE = "application/json;charset=UTF-8";
    String CODE = "code";
    String WEBSITE_CODE = "websiteCode";
    String COUNTRY_CODE = "countryCode";
    String COUNTRY = "country";

    /**
     * Get the Component identified by code
     *
     * @param code identifies the Component
     * @return a Component identified by code
     * @throws TravelFusionManagementException when the Component can't be retrieve
     */
    @GET
    @Path("/{code}")
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = false, p95ResponseTimeMillis = 50)
    Component getComponent(@PathParam(CODE) String code)
            throws TravelFusionManagementException;

    /**
     * Get all Components
     *
     * @return a list of all Components
     * @throws TravelFusionManagementException when the Components can't be retrieve
     */
    @GET
    @GZIP
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = false, p95ResponseTimeMillis = 3000)
    List<Component> getAllComponents()
            throws TravelFusionManagementException;

    /**
     * Get the ComponentVersion identified by Component, Website and Country. The Country only applies when exists a
     * specific rule.
     *
     * @param componentCode identifies the Component
     * @param websiteCode   identifies the Website
     * @param countryCode   identifies the Country
     * @return a ComponentVersion identified by websiteCode, componentCode and countryCode
     * @throws TravelFusionManagementException when the ComponentVersion can't be retrieve
     */
    @GET
    @Path("/{code}/website/{websiteCode}/country/{countryCode}/version")
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = false, p95ResponseTimeMillis = 50)
    ComponentVersion getComponentVersion(@PathParam(CODE) String componentCode,
                                         @PathParam(WEBSITE_CODE) String websiteCode,
                                         @PathParam(COUNTRY_CODE) String countryCode)
            throws TravelFusionManagementException;

    /**
     * Indicates if a Website is forcing to be merchant a Component
     *
     * @param componentCode identifies the Component
     * @param websiteCode   identifies the Website
     * @return true if is forcing to be merchant, otherwise false
     * @throws TravelFusionManagementException when the ComponentVersion can't be retrieve
     */
    @GET
    @Path("/{code}/website/{websiteCode}/forcing-to-be-merchant")
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = false, p95ResponseTimeMillis = 50)
    boolean isWebsiteForcingToBeMerchant(@PathParam(CODE) String componentCode,
                                         @PathParam(WEBSITE_CODE) String websiteCode)
            throws TravelFusionManagementException;


    /**
     * Get the AgentParameter identified by point of origin
     *
     * @param countryCode identifies the country code of the poo
     * @param componentId identifies the component
     * @return a AgentParameter by point of origin
     * @throws TravelFusionManagementException when the AgentParameter can't be retrieve
     */
    @GET
    @Path("/{code}/agentParameter/{country}")
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = true, p95ResponseTimeMillis = 700)
    AgentParameter agentParameter(@PathParam(CODE) String componentId, @PathParam(COUNTRY) String countryCode)
            throws TravelFusionManagementException, TravelFusionConfigurationNotFoundException;

    /**
     * Get all components website status
     * @return a list of component website status
     * @throws TravelFusionManagementException when list can't be retrieve
     */
    @GET
    @Path("/websiteStatus")
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = false, p95ResponseTimeMillis = 300)
    List<ComponentStatus> getAllComponentStatus() throws TravelFusionManagementException;

    /**
     * Get all website status of an specific component
     * @return a list of component website status
     * @throws TravelFusionManagementException when list can't be retrieve
     * @throws TravelFusionConfigurationNotFoundException when not found values
     */
    @GET
    @Path("/{code}/websiteStatus")
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = false, p95ResponseTimeMillis = 50)
    List<ComponentStatus> getComponentStatusByComponent(@PathParam(CODE) String componentId)
            throws TravelFusionManagementException, TravelFusionConfigurationNotFoundException;


    /**
     * Get a website status of an specific component in an specific website
     * @return a list of component website status
     * @throws TravelFusionManagementException when list can't be retrieve
     * @throws TravelFusionConfigurationNotFoundException when not found values
     */
    @GET
    @Path("/{code}/websiteStatus/{websiteCode}")
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = false, p95ResponseTimeMillis = 50)
    ComponentStatus getComponentStatusByComponentAndWebsite(@PathParam(CODE) String componentId,
                                                            @PathParam(WEBSITE_CODE) String websiteCode)
            throws TravelFusionManagementException, TravelFusionConfigurationNotFoundException;
}
