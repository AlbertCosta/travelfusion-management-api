package com.odigeo.travelfusionmanagementapi.component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComponentVersion implements Serializable {

    private static final long serialVersionUID = -6609132514347593710L;

    private String version;
    private PrepaymentType prepayment;
    private boolean bspCash;
}
