package com.odigeo.travelfusionmanagementapi.route;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode
public class Route implements Serializable {

    private static final long serialVersionUID = -4201894142243222018L;

    private static final String DELIMITER = "-";
    private String departureCity;
    private String arrivalCity;

    @JsonCreator
    public Route(String encoded) {
        decode(encoded);
    }

    @JsonValue
    @Override
    public String toString() {
        return encode();
    }

    private String encode() {
        return this.getDepartureCity() + DELIMITER + this.getArrivalCity();
    }

    private void decode(String encoded) {
        int delimiterIndex = encoded.indexOf(DELIMITER);
        this.setDepartureCity(encoded.substring(0, delimiterIndex));
        this.setArrivalCity(encoded.substring(delimiterIndex + 1));

    }

}
