package com.odigeo.travelfusionmanagementapi.route;

import com.odigeo.commons.rest.ServiceGroup;
import com.odigeo.commons.rest.metadata.Metadata;
import com.odigeo.travelfusionmanagementapi.exception.TravelFusionManagementException;
import org.jboss.resteasy.annotations.GZIP;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;
import java.util.Set;

import static com.odigeo.travelfusionmanagementapi.component.ComponentService.JSON_MIME_TYPE;

@Path("/v1/route")
@ServiceGroup("TravelFusionManagement")
public interface RouteService {


    /**
     * Get the all the routes with the served components
     *
     * @return a map for route with all the components for that route
     * @throws TravelFusionManagementException when the Component can't be retrieve
     */
    @GET
    @Path("/component")
    @Produces({JSON_MIME_TYPE})
    @GZIP
    @Metadata(isIdempotent = true, p95ResponseTimeMillis = 15000)
    Map<Route, Set<String>> getAllComponentsByRoutes()
            throws TravelFusionManagementException;

}
