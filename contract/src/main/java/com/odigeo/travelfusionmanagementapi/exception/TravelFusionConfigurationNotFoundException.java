package com.odigeo.travelfusionmanagementapi.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("TravelFusionConfigurationNotFoundException")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TravelFusionConfigurationNotFoundException extends Exception {

    public TravelFusionConfigurationNotFoundException(String s) {
        super(s);
    }

    public TravelFusionConfigurationNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
