package com.odigeo.travelfusionmanagementapi.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("TravelFusionManagementException")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TravelFusionManagementException extends Exception {

    public TravelFusionManagementException(String message) {
        super(message);
    }

    public TravelFusionManagementException(String message, Throwable cause) {
        super(message, cause);
    }
}
