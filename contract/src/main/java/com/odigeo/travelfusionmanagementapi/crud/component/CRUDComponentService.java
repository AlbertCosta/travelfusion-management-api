package com.odigeo.travelfusionmanagementapi.crud.component;

import com.odigeo.commons.rest.ServiceGroup;
import com.odigeo.commons.rest.metadata.Metadata;
import com.odigeo.travelfusionmanagementapi.exception.TravelFusionConfigurationNotFoundException;
import com.odigeo.travelfusionmanagementapi.exception.TravelFusionManagementException;
import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.cache.Cache;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/v1/crud/component")
@ServiceGroup("TravelFusionManagement")
@Cache(maxAge = 1800)
public interface CRUDComponentService {

    String JSON_MIME_TYPE = "application/json;charset=UTF-8";
    String CODE = "code";

    /**
     * Get the Component identified by code
     *
     * @param code identifies the Component
     * @return a Component identified by code
     * @throws TravelFusionManagementException when the Component can't be retrieve
     */
    @GET
    @Path("/{code}")
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = true, p95ResponseTimeMillis = 5000)
    Component getComponent(@PathParam(CODE) String code)
            throws TravelFusionManagementException, TravelFusionConfigurationNotFoundException;

    /**
     * Get all Components
     *
     * @return a list of all Components
     * @throws TravelFusionManagementException when the Components can't be retrieve
     */
    @GET
    @GZIP
    @Produces({JSON_MIME_TYPE})
    @Metadata(isIdempotent = true, p95ResponseTimeMillis = 5000)
    List<Component> getAllComponents()
            throws TravelFusionManagementException;

}
