package com.odigeo.travelfusionmanagementapi.crud.component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.odigeo.travelfusionmanagementapi.ProductType;
import com.odigeo.travelfusionmanagementapi.component.ComponentVersion;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Component implements Serializable {

    private static final long serialVersionUID = 2401795956145249875L;

    private String code;
    private ProductType productType;
    private int maxNumberOfBags;
    private int minNumberOfBags;
    private int maxBaggageWeight;
    private int minBaggageWeight;
    private String gdsIdBackup;
    private boolean api3DSecure;
    private boolean merchant3DSecure;
    private boolean merchantAllowed;

    private boolean isTest;
    private Set<String> websitesAlwaysMerchant;
    private Map<String, ComponentVersion> websiteToVersion;
    private Set<ComponentVersion> allVersions;
    private Map<String, Map<String, ComponentVersion>> countriesOrWebsitesWithNoValidCurrencyToVersion;
    private Set<String> providerCreditCardTypes;
    private Map<String, Set<String>> websiteCreditCardAllowed;
    private Set<String> websitesActive;
}
