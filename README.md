# TravelFusion management API

This library is the communication interface for travelfusion-management module.

## Change log
The list of all changes can be found [here](CHANGELOG.md).

## Pull Request acceptance criteria

#### Before starting a new development
Send an email to [lowcost-gang-dev](mailto://lowcost-gang-dev@edreamsodigeo.com) explaining the changes and the ticket number to solve any doubt.

#### Do we want to add a new service?
Any new method/service that wants to be added should be implemented also in *mock* folder.

#### During the development
1. Create a TFMAPI-XXX Jira and relate with your task (TFMAPI project is the APP jira for this library)
2. All commits must contain the ticket number (in capital letters : log-123 is wrong, LOG-123 is correct)

#### Can I do the Pull Request?
The following list must be completed:

1. No dependency with SNAPSHOT version must exist in pom.xml, only final versions are allowed.
2. No conflicts.
3. Jenkins CI job must be OK (green light)
4. At least a member of [LowcOst Gang POD](https://jira.odigeo.com/wiki/display/KB/Lowcost+Gang) and a member of your POD should approved the Pull Request

#### Pull Request procedure
1. Create a pull request in Bitbucket adding the Jira ticket in the title
2. Move your own ticket to "Pull Request" (Not the TFMAPI-XXX)
3. Assign the ticket to "Low Cost Gang DEV"
