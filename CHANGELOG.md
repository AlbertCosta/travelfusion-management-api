## Change log

### 1.20.0
* [LOG-1179](https://jira.odigeo.com/browse/LOG-1179): Make Route object serializable

### 1.19.0
* [LOG-1178](https://jira.odigeo.com/browse/LOG-1178): Add components by routes endpoint (Moving logic from TFGW to TFMG)

### 1.18.2
* Add mandatory annotations to new operations & tag release

### 1.18.0
* [LOG-1173](https://jira.odigeo.com/browse/LOG-1173): Add component status by website (Moving from TFGW to TFMG)

### 1.17.0
* [LOG-1151](https://jira.odigeo.com/browse/LOG-1151): Enable fault tolerance for agentParameter API

### 1.16.0
* [LOG-1164](https://jira.odigeo.com/browse/LOG-1164): Include tests in CI & remove local cache when redis is available

### 1.14.0
* [LOG-1117](https://jira.odigeo.com/browse/LOG-1117): Makes cache configurable (redis / local)

### 1.10.0
* [LOG-975](https://jira.odigeo.com/browse/LOG-975): Add cache to CRUD implementation and Redis cache

### 1.9.0
* [LOG-978](https://jira.odigeo.com/browse/LOG-978): Remove unused code in CRUD implementation

### 1.8.2
* Fix last classifier artifact

### 1.8.0
* [LOG-956](https://jira.odigeo.com/browse/LOG-956): Rename endpoints

### 1.7.4
* [LOG-889](https://jira.odigeo.com/browse/LOG-889): Add BSP CASH field

### 1.7.3
* [LOG-911](https://jira.odigeo.com/browse/LOG-911): Fix missing commonsrest packaging

### 1.7.2
* [LOG-887](https://jira.odigeo.com/browse/LOG-887): Fix merge sources into unique jar
* [LOG-883](https://jira.odigeo.com/browse/LOG-883): Fix versioned and last sources
* [LOG-877](https://jira.odigeo.com/browse/LOG-877): Apply new API standard

### 1.6.0
* [LOG-794](https://jira.odigeo.com/browse/LOG-794): Migrate module to GIT
